#!/usr/bin/python
# -*- coding: utf-8 -*-
import pygame
from pygame.locals import *
import time
import random, sys
from random import shuffle

#-------Colores---------
BLANCO = (255, 255, 255)
NEGRO = (0, 0, 0)
ROJO = (255, 0, 0)
AZUL = (0, 0, 255)
VERDE = (0, 255, 0)
PLOMO = (243,224,224)
AMARILLO = (255,255,0)
#----------------------
display_ancho = 950
display_altura = 700
#----------------------
PATH_CARTAS = "/Users/Kelvin/Desktop/Universidad/Estructuras Discretas I/BlackJack"
cartas_en_juego = []
cartas_jugador = pygame.sprite.Group()
cartas_crupier = pygame.sprite.Group()
carta_v = pygame.sprite.GroupSingle()
cuentaJ = 0
cuentaC = 0
cuentaJugador = 0
cuentaDealer = 0
resetJugador = False
resetDealer = False
st = False
pygame.init()
#---Para interfaz ----
Turno = 1
dimensiones = (display_ancho, display_altura)
pantalla = pygame.display.set_mode(dimensiones)
pygame.display.set_caption("BLACKJACK")
reloj = pygame.time.Clock()
font = pygame.font.SysFont(None, 25)
pequenafuente = pygame.font.SysFont("comicsansms",15)
medianafuente = pygame.font.SysFont("comicsansms",30)
grandefuente = pygame.font.SysFont("comicsansms",80)
#Datos de boton
Boton1 = [360,200]
TamBoton1 = [200,50]
ColorBoton1 = [PLOMO, AMARILLO]
#----------------------------------------
Boton2 = [360,300]
ColorBoton2 = [PLOMO, AZUL]
#----------------------------------------
Boton3 = [360,400]
ColorBoton3 = [PLOMO, VERDE]
#----------------------------------------
Boton4 = [360,500]
ColorBoton4 = [PLOMO, ROJO]
#-----------------------------------------
def TextoBoton(msg,color,BotonX,BotonY,Ancho,Alto, tamano="pequeno"):
    textoSuperficie, textoRect = objetoTexto(msg,color,tamano)
    textoRect.center = (BotonX+(Ancho/2),BotonY+(Alto/2))
    pantalla.blit(textoSuperficie,textoRect)

#----Función para cargar imágenes----
def load_image(filename, transparent=False):
        try: image = pygame.image.load(filename)
        except (pygame.error, message):
                raise (SystemExit, message)
        image = image.convert()
        if transparent:
                color = image.get_at((0,0))
                image.set_colorkey(color, RLEACCEL)
        return image
#--------Función para los botones-----------
def botones(texto,superficie,estado,Posicionamiento,tam,identidad = None):
    cursor = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    
    
    #if Posicionamiento[1] + tam[1] > cursor[0] 
    if Posicionamiento[0] + tam[0] > cursor[0] > tam[0] and Posicionamiento[1] + tam[1] > cursor[1] > tam[1] and Posicionamiento[1] + tam[1] < cursor[1] + tam[1] and Posicionamiento[0] + tam[0] < cursor[0] + tam[0]:
        if click[0] == 1:
            if identidad == "comienzo":
                gameLoop()
                tiempo = time.sleep()
            elif identidad == "ayuda":
                pass
            elif identidad == "hit":
                    
                hit(len(cartas_jugador))
            elif identidad == "reiniciar":
                 cartas_jugador.empty()
                 cartas_crupier.empty()
                 global resetJugador
                 global resetDealer
                 resetJugador = True
                 resetDealer = True
                 gameLoop()
            elif identidad == "stand":
                carta_v.empty()
                global st
                st = True
                stand(len(cartas_crupier))
                
            elif identidad == "salir":
                quit()
        boton = pygame.draw.rect(superficie,estado[1],(Posicionamiento[0],Posicionamiento[1],tam[0],tam[1]))
            
    else:
        boton = pygame.draw.rect(superficie,estado[0],(Posicionamiento[0],Posicionamiento[1],tam[0],tam[1]))
    TextoBoton(texto,NEGRO,Posicionamiento[0], Posicionamiento[1],tam[0],tam[1],tamano="mediano")
    return boton
#-----------------------------------------
background_image = load_image('fondo.png')
pantalla.blit(background_image, (0,0))
pygame.display.flip()
#--------------------------------
##def botones(superficie,color,Pos1,Pos2,tam1, tam2):
##    boton = pygame.draw.rect(superficie,color,(Pos1,Pos2,tam1,tam2))
##    return boton
#--------------------------------
def objetoTexto(texto,color,tamano):
    if tamano == "pequeno":
        textoSuperficie = pequenafuente.render(texto,True,color)
    if tamano == "mediano":
        textoSuperficie = medianafuente.render(texto,True,color)
    if tamano == "grande":
        textoSuperficie = grandefuente.render(texto,True,color)
    return textoSuperficie, textoSuperficie.get_rect()
#-------Función para mostrar un mensaje-------
def mensaje(msg,color,desplazamientoY=0, tamano="pequeno"):
    textoSuperficie, textoRect = objetoTexto(msg,color,tamano)
    textoRect.center = (display_ancho/2),(display_altura/2) + desplazamientoY
    pantalla.blit(textoSuperficie,textoRect)
#----Función que selecciona las cartas-------
def seleccionar_carta():

    cambiar = True
    carta = ""
    palos = {0:"H", 1:"S", 2:"D", 3:"C"}
    while cambiar:
        x = random.randrange(0,4)
        palo = palos[x]
        y = int(random.randrange(2,15))
        carta = str(y) + palo
        if y >= 10 and y <=13:
            j = "K"
        elif y<10:
            j = str(y)
        else:
            j = "A"
        puntosJ = pointCount(j)
        if len(cartas_en_juego) != 0:
            for a in cartas_en_juego:
                if carta == a:
                    break
        cambiar = False
    cartas_en_juego.append(carta)
    
    return [PATH_CARTAS + "/" + palo + "/" + str(y) + ".png", puntosJ]
#--------------------------------------------
def aces(myCount):
    if(myCount >= 10):
        myCount += 11
    elif(aceCount != 0):
        myCount += 1

class cartaVolteada(pygame.sprite.Sprite):
    
    def __init__(self):
        
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("cardBack_red5_opt.png").convert_alpha()
        self.rect = self.image.get_rect()

        self.rect.centery = 100
class carta(pygame.sprite.Sprite):

    def __init__(self):
            path1, cuentaJ = seleccionar_carta()
            global cuentaJugador
            global resetJugador
            ases= 0
            if resetJugador == True:
                cuentaJugador = 0
                cuentaJugador = cuentaJ
                ases= 0
                resetJugador = False
            elif cuentaJugador >= 11 and cuentaJ == 1:
                cuentaJugador = cuentaJugador + cuentaJ
            elif cuentaJugador <= 10 and cuentaJ == 1 and ases == 0:
                cuentaJugador = cuentaJugador + 11
                ases = ases + 1
            elif ases>0:
                cuentaJugador = cuentaJugador + 1
            else:
                cuentaJugador = cuentaJugador + cuentaJ
            
            print("Jugador: " + str(cuentaJugador))
            pygame.sprite.Sprite.__init__(self)
            # Cuando creamos la carta seleccionamos una al azar
            self.image = pygame.image.load(path1).convert_alpha()
            self.rect = self.image.get_rect()

            self.rect.centery = 500
class cartaCrupier(pygame.sprite.Sprite):

    def __init__(self):
            path2, cuentaC = seleccionar_carta()
            global cuentaDealer
            global resetDealer
            global st
            ases= 0
            if resetDealer == True:
                cuentaDealer = 0
                cuentaDealer = cuentaC
                ases=0
                st = False
                resetDealer = False
            elif cuentaDealer >= 11 and cuentaC == 1:
                cuentaDealer = cuentaDealer + cuentaC
            elif cuentaDealer <= 10 and cuentaC == 1 and ases == 0:
                cuentaDealer = cuentaDealer + 11
                ases=ases+1
            elif ases > 1:
                cuentaDealer = cuentaDealer + 1
            else:
                cuentaDealer = cuentaDealer + cuentaC
            print("Dealer: " + str(cuentaDealer))
            pygame.sprite.Sprite.__init__(self)
            # Cuando creamos la carta seleccionamos una al azar
            self.image = pygame.image.load(path2).convert_alpha()
            
            while cuentaDealer <= 16 and st == True:
                self.image = pygame.image.load(path2).convert_alpha()
                
            self.rect = self.image.get_rect()
            self.rect.centery = 100
class flecha(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("side-down.png").convert_alpha()
        self.rect = self.image.get_rect()

        self.rect.centery = 500

##def turno(turno):
##    flecha_turno = flecha()
##    flecha = pygame.sprite.Group()
##    if turno == 1:       
##        flecha_turno.rect.left = 130
##    elif turno == 2:
##        flecha_turno.rect.left = 500
##    elif turno == 3:
##        flecha_turno.rect.left = 900
##    flecha.add(flecha_turno)
##    flecha.draw(pantalla)
def hit(cant):
    global cartas_jugador
    c2 = carta()
    #print(cant)
    c2.rect.left = (cant) * 90 +400
    cartas_jugador.add(c2)
    cartas_jugador.draw(pantalla)
def stand(cant):
    global cartas_crupier
    cc = cartaCrupier()
    cc.rect.left = (cant) * 90 + 400
    cartas_crupier.add(cc)
    cartas_crupier.draw(pantalla)
#-------------Total de puntos----------
def pointCount(myCards):
    myCount = 0
    aceCount = 0
    for i in str(myCards):
        if(i[0] == "K"):
            myCount += 10
        elif(i[0] != "A"):
            myCount += int(i[0])
        else:
            aceCount += 1

    if(aceCount == 1 and myCount >= 10):
        myCount += 11
    elif(aceCount != 0):
        myCount += 1

    return myCount
#-------------------------------------------
def deck():
    deck = ""
    for suit in ['H','S','D','C']:
        for rank in ['A','2','3','4','5','6','7','8','9','T','J','Q','K']:
            deck = (suit+rank)

    shuffle(deck)

    return deck
#----- Introducción del juego---------------
def introduccion():
    
    introJuego = True

    while introJuego:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                introJuego = False
                quit()
        pantalla.fill(VERDE)
##        background_image = load_image('fondo.png')
##        pantalla.blit(background_image, (0,0))
        pygame.display.flip()
        mensaje("BLACKJACK", NEGRO, -200, tamano="grande")
        botones("Jugar",pantalla,ColorBoton1, Boton1, TamBoton1,identidad="comienzo")
        #botones("Opciones",pantalla,ColorBoton2, Boton2, TamBoton1, identidad="opciones")
        botones("Ayuda",pantalla,ColorBoton3, Boton2, TamBoton1,identidad="creditos")
        botones("Salir",pantalla,ColorBoton4, Boton3, TamBoton1,identidad="salir")

        pygame.display.update()
        reloj.tick(30)

def gameLoop():
    gameExit = False
    gameOver = False
    global cartas_jugador
    global cartas_crupier
    global carta_v
    
    cv = cartaVolteada()
    cv.rect.left = 2*45+400
    carta_v.add(cv)
    cPC = cartaCrupier()
    cPC.rect.left = 0*90+400
    for a in range(2):
        ##c = carta()
        c2 = carta()
        ##c3 = carta()
        
        
        ##c.rect.left = a*90+30
        c2.rect.left = a*90+400
        ##c3.rect.left = a*90+750
        
##        cartas_jugador.add(c)
        cartas_jugador.add(c2)
        ##cartas_jugador.add(c3)
        cartas_crupier.add(cPC)
    while not gameExit:
       
        while gameOver == True:
            mensaje("Se agotaron las cartas", NEGRO, 300, tamano="mediano")
            pygame.display.update()
            
            
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameExit = True
                quit()
        background_image = load_image('fondo.png')
        pantalla.blit(background_image, (0,0))
        pygame.draw.rect(pantalla, (132,66,30), [0,600,1080,100])
        botones("Hit",pantalla,ColorBoton3, (680,630), (100,50),identidad="hit")
        botones("Stand",pantalla,ColorBoton4, (800,630), (100,50),identidad="stand")
        botones("Reiniciar",pantalla,ColorBoton4, (450,630), (100,50),identidad="reiniciar")
        cartas_jugador.draw(pantalla)
        cartas_crupier.draw(pantalla)
        carta_v.draw(pantalla)
        
        #dealerCount = pointCount(cartas_crupier.sprites())
        #print(playerCount)
        #print(cartas_jugador.sprites())
        pygame.display.flip()

    
#gameLoop()
introduccion()
