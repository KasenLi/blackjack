import pygame
import random
from pygame.locals import *
import sys

PATH_CARTAS ="/Users/Kelvin/Desktop/Universidad/Estructuras Discretas I/Cartas"

def seleccionar_carta():
    #   Definimos un diccionario con los mazos
    palos = {0:"corazon", 1:"pica", 2:"diamante", 3:"trebol"}
    x = random.randrange(0,4)
    palo = palos[x]
    y = random.randrange(2, 15)
    #   Retornamos el path
    return PATH_CARTAS + "/" + palo + "/" + str(y) + ".png"
class carta(pygame.sprite.Sprite):
        def __init__(self):
                pygame.sprite.Sprite.__init__(self)
                #   Cuando creamos la carta seleccionamos una al azar
                self.image = pygame.image.load(seleccionar_carta()).convert_alpha()
                self.rect = self.image.get_rect()
                #   Esto es solo por ahora
                self.rect.centery = 350

def main():
        screen = pygame.display.set_mode((900, 700))
        pygame.display.set_caption("Juego cartas - ANCODI VideoGames")
        #   Creamos un grupo para las cartas
        cartas_jugador = pygame.sprite.Group()
    
        #   Vamos a crear 5 cartas por el momento
        #   para ver el resultado
        for a in range(5):
                c = carta()
                #   Esta posición es momentanea
                #   Es solo para ver el resultado mejor
                #   Por lo que no se explicará los números
                print(a)
                c.rect.left = a*140+30
                #   Se añade al grupo
                cartas_jugador.add(c)
    
        while True:
                #   Eventos
                for evento in pygame.event.get ():
                        if evento.type == pygame.QUIT:
                                sys.exit()
                
                #   Un fondo naranja
                screen.fill((235,135,45))

                cartas_jugador.draw(screen)

                pygame.display.flip()
main()
