from random import shuffle
import pygame
import time
from tkinter import *

#        i=i+1
#Variables

#Una función para crear una baraja
#Suits: H, S, D, C - Rank: A, 2-9, T, J, Q, K
#Retorna una baraja de 52 cartas barajadas
        
def deck():
    deck = []
    for suit in ['H','S','D','C']:
        for rank in ['A','2','3','4','5','6','7','8','9','T','J','Q','K']:
            deck.append(suit+rank)

    shuffle(deck)

    return deck

#Una función para contar los puntos
#Toma de las cartas del jugador y retorna su total de puntos
def pointCount(myCards):
    myCount = 0
    aceCount = 0
    for i in myCards:
        if(i[1] == 'J' or i[1] == 'Q' or i[1] == 'K' or i[1] == 'T'):
            myCount += 10
        elif(i[1] != 'A'):
            myCount += int(i[1])
        else:
            aceCount += 1

    if(aceCount == 1 and myCount >= 10):
        myCount += 11
    elif(aceCount != 0):
        myCount += 1

    return myCount

#Una función para crear los jugadores y crupier
#Entrega al azar dos cartas de la baraja a cada uno
#Retorna una lista con ambas manos
def createPlayingHands(myDeck):
    dealerHand = []
    playerHand = []
    dealerHand.append(myDeck.pop())
    dealerHand.append(myDeck.pop())
    playerHand.append(myDeck.pop())
    playerHand.append(myDeck.pop())

    while(pointCount(dealerHand) <= 16):
        dealerHand.append(myDeck.pop())

    return [dealerHand, playerHand]

#Aqui creamos las cosas del juego
#Ciclo del juego
game = ""
myDeck = deck()
hands = createPlayingHands(myDeck)
dealer = hands[0]
player = hands[1]

while(game != "exit"):
    dealerCount = pointCount(dealer)
    playerCount = pointCount(player)
    
    print ("Dealer has:")
    print (dealer[0])

    print ("Player1, you have:")
    print (player)
    print(playerCount)    
    if(playerCount == 21):
        print ("Blackjack! Player win!!")
        break
    elif(playerCount > 21):
        print ("Player busts! With " + str(playerCount) + " points. Dealer wins!")
        break
    elif(dealerCount > 21):
        print ("Dealer busts! With " + str(dealerCount) + " points. Player wins!")
        break
    
    game = input("What would you like to do? H: Hit me, S: Stand? ")

    if(game == 'H'):
        player.append(myDeck.pop())
    elif(playerCount > dealerCount):
        print ("Player wins with " + str(playerCount) + " points")
        print ("Dealer has: " + str(dealer) + " or " + str(dealerCount) + "points")
        break
    else:
        print ("Dealer wins!")
        print ("Dealer has: " + str(dealer) + " or " + str(dealerCount) + "points")
        break
pygame.quit()
